const mongoose = require('mongoose');

const connectToDB = async (dbURI, config) => {
    try {
        await mongoose.connect(dbURI, config);
    } catch (err) {
        console.log(err)
        process.exit(-1);
    }
};
module.exports = connectToDB;