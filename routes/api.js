const router = require('express').Router();
const productsController = require('../app/controllers/productsController');
const categoriesController = require('../app/controllers/categoriesController');
const subcategoriesController = require('../app/controllers/subcategoriesController');

/* products routes */
router.get('/products/get-all', productsController.getAll);
router.get('/products/get-by-category-id/:id', productsController.getProductsByCategoryId);
router.get('/products/get-by-subcategory-id/:id', productsController.getProductsBySubcategoryId);
router.post('/products/add', productsController.addProduct);

/* categories routes */
router.get('/category/get-all', categoriesController.getCategories);
router.post('/category/add', categoriesController.addCategory);
router.post('/category/remove', categoriesController.removeCategory);

/* subcategories routes */
router.post('/subcategory/add', subcategoriesController.addSubcategory);

module.exports = router;
