const createError = require('http-errors');
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const connectToDB = require('../utils/db');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const indexRouter = require('../routes');
const apiRouter = require('../routes/api');

const app = express();
const server = require('http').createServer(app);
const {Server} = require('socket.io');
const io = new Server(server, {
  cors: {
    origin: ["http://localhost:4200", "http://localhost:8100"],
    methods: ["GET", "POST"],
    credentials: true
  }
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8100');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
const whitelist = ['http://localhost:4200', 'http://localhost:8100'];
const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  },
  optionsSuccessStatus: 200,
};
app.use(cors(corsOptions))
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  // res.render('error');
});

(async function () {
  await connectToDB('mongodb+srv://admin:fuGI6hzlgzsXJYxU@joycity.glbqb.mongodb.net/joycity?retryWrites=true&w=majority', {
    autoIndex: false, // Don't build indexes
    serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4, // Use IPv4, skip trying IPv6
    useNewUrlParser: true,
    useUnifiedTopology: true,
    promiseLibrary: global.Promise
  });
}());

mongoose.connection.on('connected', async () => {
  io.on('connection', async (socket) => {
    console.info(`${socket.id} connected.`);
    socket.emit('order', {success: true});
  });
  server.listen(process.env.PORT, () => {
    app.use((req, res, next) => {
    });
  });
});

module.exports = app;
