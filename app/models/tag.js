const mongoose = require('mongoose');
const {Schema} = mongoose;

const tagSchema = new Schema(
  {
    title: {type: String, required: true}
  },
  {collection: 'tag', versionKey: false, autoCreate: true},
);

tagSchema.virtual('id').get(function(){
  return this._id.toHexString();
});

tagSchema.set('toJSON', {
  virtuals: true
});

const Tag = mongoose.model('tag', tagSchema);
module.exports = {Tag, tagSchema}
