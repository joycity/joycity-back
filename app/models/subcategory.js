const mongoose = require('mongoose');
const {Schema} = mongoose;

const subcategorySchema = new Schema(
  {
    title: {type: String, required: true},
    category: {type: mongoose.Types.ObjectId, ref: 'category'},
  },
  {collection: 'subcategory', versionKey: false, autoCreate: true},
);

subcategorySchema.virtual('id').get(function(){
  return this._id.toHexString();
});

subcategorySchema.set('toJSON', {
  virtuals: true
});

const Subcategory = mongoose.model('subcategory', subcategorySchema);
module.exports = {Subcategory, subcategorySchema}
