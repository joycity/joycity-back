const mongoose = require('mongoose');
const {Schema} = mongoose;

const categorySchema = new Schema(
  {
    title: {type: String, required: true},
    subcategories: [{type: mongoose.Types.ObjectId, ref: 'subcategory'}],
  },
  {collection: 'category', versionKey: false, autoCreate: true},
);

categorySchema.virtual('id').get(function(){
  return this._id.toHexString();
});

categorySchema.set('toJSON', {
  virtuals: true
});

const Category = mongoose.model('category', categorySchema);
module.exports = {Category, categorySchema}
