const mongoose = require('mongoose');
const {Schema} = mongoose;

const productSchema = new Schema(
  {
    title: {type: String, required: true},
    categoryId: {type: mongoose.Types.ObjectId, ref: 'category', required: true},
    subcategoryId: {type: mongoose.Types.ObjectId, ref: 'subcategory', required: true},
    price: {type: Number, required: true},
    discount: {type: Number, default: 0},
    inventory: {type: Number, default: 0},
    images: [{type: String}],
    tags: [{type: mongoose.Types.ObjectId, ref: 'tag'}]
  },
  {collection: 'product', versionKey: false, autoCreate: true},
);

productSchema.virtual('id').get(function(){
  return this._id.toHexString();
});

productSchema.set('toJSON', {
  virtuals: true
});

const Product = mongoose.model('product', productSchema);
module.exports = {Product, productSchema}
