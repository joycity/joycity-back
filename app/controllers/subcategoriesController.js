const subcategoriesService = require('../services/subcategoriesService')

class SubcategoriesController {
  async getProductsById(req, res) {
    const result = await subcategoriesService.findByData({}, 'category');
    const count = await subcategoriesService.getCount({});
    res.send({
      result,
      count
    });
  }

  async addSubcategory(req, res) {
    const result = await subcategoriesService.addSubcategory(req.body);
    res.send({
      result
    });
  }
}

module.exports = new SubcategoriesController();