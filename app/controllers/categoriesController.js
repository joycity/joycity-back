const categoriesService = require('../services/categoriesService')

class CategoriesController {
  async getCategories(req, res) {
    const result = await categoriesService.findByData({}, 'subcategories');
    const count = await categoriesService.getCount({});
    res.send({
      result,
      count
    });
  }

  async getSubcategoriesByCategoryId(req, res) {
    const result = await categoriesService.findByData({_id: req.params.id}, 'subcategories');
    res.send({
      result
    });
  }

  async addCategory(req, res) {
    const result = await categoriesService.addCategory(req.body);
    res.send({
      result
    });
  }

  async removeCategory(req, res) {
    const result = await categoriesService.removeCategory(req.body);
    res.send({
      result
    });
  }
}

module.exports = new CategoriesController();