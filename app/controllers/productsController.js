const productService = require('../services/productsService')
const productsService = require("../services/productsService");

class ProductsController {
  async getAll(req, res) {
    const result = await productService.findByData({}, 'category subcategory');
    const count = await productService.getCount({});
    res.send({
      result,
      count
    });
  }

  async getProductsByCategoryId(req, res) {
    const result = await productsService.findByData({category: req.body.categoryId});
    res.send({
      result
    });
  }

  async getProductsBySubcategoryId(req, res) {
    const result = await productsService.findByData({subcategory: req.body.subcategoryId});
    res.send({
      result
    });
  }

  async addProduct(req, res) {
    const result = await productsService.addProduct(req.body);
    res.send({
      result
    });
  }
}

module.exports = new ProductsController();