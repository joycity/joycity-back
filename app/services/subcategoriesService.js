const subcategoryModel = require('../models/subcategory');
const mongoose = require("mongoose");
const categoriesService = require("../services/categoriesService");

class SubcategoriesService {
  async findByData(data, populate = '') {
    return subcategoryModel.Subcategory.find(data).populate(populate);
  }

  async getCount() {
    return subcategoryModel.Subcategory.countDocuments({});
  }

  async addSubcategory(data) {
    const doc = {
      title: data.title,
      category: mongoose.Types.ObjectId(String(data.categoryId))
    }
    const rec = await subcategoryModel.Subcategory.create(doc);
    await categoriesService.addSubcategoryToCategory(rec.category, rec._id);
    return rec;
  }
}

module.exports = new SubcategoriesService();