const categoryModel = require('../models/category');
const mongoose = require("mongoose");

class CategoriesService {
  async findByData(data, populate = '') {
    return categoryModel.Category.find(data).populate(populate);
  }

  async getCount(data) {
    return categoryModel.Category.countDocuments(data);
  }

  async addCategory(data) {
    const doc = {
      title: data.title,
      subcategories: []
    }
    return categoryModel.Category.create(doc);
  }

  async removeCategory(data) {
    return categoryModel.Category.findByIdAndRemove(data.id);
  }

  async addSubcategoryToCategory(categoryId, subcategoryId) {
    await categoryModel.Category.findByIdAndUpdate(mongoose.Types.ObjectId(String(categoryId)), {
      $push: {
        subcategories: mongoose.Types.ObjectId(String(subcategoryId))
      }
    })
  }
}

module.exports = new CategoriesService();