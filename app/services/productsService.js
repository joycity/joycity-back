const productModel = require('../models/product');

class ProductsService {
  async findByData(data, populate = '') {
    return productModel.Product.find(data).populate(populate);
  }

  async getCount(data) {
    return productModel.Product.countDocuments(data);
  }

  async addProduct(data) {
    return productModel.Product.create({
      title: data.title,
      categoryId: data.categoryId,
      subcategoryId: data.subcategoryId,
      price: data.price,
      discount: data.discount || 0,
      inventory: data.inventory || 0,
      images: data.images || [],
      tags: data.tags || []
    });
  }
}

module.exports = new ProductsService();